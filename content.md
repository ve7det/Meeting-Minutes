--- 
# ----- Beginning Details --------------------
team: VECTOR Board
date: 22 July 2020
day: Wednesday
chair: "Jessé Neri VE7DET"
time: "19:04"
location: on Zoom Teleconference System
purpose: To manage governance of the Society

# ----- Attendance --------------------
president: yes
secretary: yes
vicepresident: yes
treasurer: yes
director1: no
director2: no
director3: yes  
director4: yes  
director5: yes	
members:
- None
external:
- None

# ----- Quorum --------------------
quorum:
- "7 board: Quorum at 19:10 (4 table officers and 3 directors at large)"
- "7 board members present from 19:04 to 20:05"

# ----- Confirming Agenda --------------------
agenda:
- Treasurer Transition - replace with 2021 Funding
- 2021 funding
- Spending Allocations 2020
- Directors' Liability Insurance
- Tester Insurance
- MOU Report
- AGM Minutes and Bylaw Updates
- CERV Future

# ----- Consent Agenda Notes --------------------
consentagendanotes:
- none
---



---
agenda1: Treasurer Transition
advocate1: Anthony VE7XAK
initial1: I poropose that VECTOR only use our current grant money and resources in 2021, without applying for additional funds.
notes1:
- 
mover1: 
seconder1: 
approved1: 
action1a: 
actor1a: 
deadline1a: 
---



---
agenda2: Spending 
advocate2: Jesse
initial2: I propose we choose spending buckets and create project planning forms for each one to prepare to spend our 2020 Gaming Grant.
notes2:
- Grab and Go team - major components and parts for 4 more kits
- Refurbished Lenovo T440 laptops with Microsoft Approved Refurbisher Windows 10
- New Packet Radio
- Radio Room Tools and Testing Gear
revised2: 
mover2: 
seconder2: 
approved2: 
action2a: 
actor2a: 
deadline2a: 
action2b: 
actor2b: 
deadline2b: 
---


---
agenda3: Tester Insurance
advocate3:  Jesse VE7DET
initial3: "I propose that we investigate costs for carrying equipment insurance that allows us to enlist testers for gear from trusted members in the broader radio community.  Perhaps to cover a $3000 loss. "
notes3:
- 
mover3: 
seconder3: 
approved3: 
action3a: 
actor3a: 
deadline3a: 
---


---
agenda4: MOU Report
advocate4:  Jesse VE7DET
initial4: "I propose that I send our MOU Partners the report stored in my MOUReport git repository. (https://git.vectorradio.ca/VE7DET/Report-MOUPartners)"
notes4:
- 
mover4: 
seconder4: 
approved4: 
action4a: 
actor4a: 
deadline4a: 
---


---
agenda5: AGM Minutes and Bylaw Updates
advocate5:  
initial5: I propose that we circulate the AGM minutes and bylaw updates
notes5:
- 
mover5: 
seconder5: 
approved5: 
action5a: 
actor5a: 
deadline5a: 
---


---
agenda6: CERV Future
advocate6:
initial6: I propose that VECTOR strike a team by the 2020 AGM to activate a replacement for the CERV and retire the GM bus platform by the 2021 Annual General Meeting.   
notes6:
- 
mover6: 
seconder6: 
approved6: 
action6a: 
actor6a: 
deadline6a: 
---


---
agenda7: 
advocate7:
initial7: 
notes7:
- 
mover7: 
seconder7: 
approved7: 
action7a: 
actor7a: 
deadline7a: 
---


---
# ----- CLOSING ITEMS --------------------
parkinglot:
- (all other items parked for next meeting)
- Directors' Liability Insurance
- Tester Insurance 
- Darryl indicates willing to take over managing website, Harondel requests that SOP be written for site management.
reflections:
- Jesse - concerns about lack of progress 
- Anthony - recommends discussing items over email between board meetings
- Linda - concerns about 
- Tyson - good that moved through weighty topics, good that Jesse wants to involve membership
- Harondel - happy medium between meetings and email in between
- Darryl - some amount of emails in between would be good to reduce discussion time during meeting
- Prem - glad got as far as we did, interviews with Fire and Police and how they surviced when commms went down, review manual for inter comms
- Linda - good meeting, lots to do and discussion, use email intermittently, need to come up with process to be more produtive at meetings
- Darryl requests Zoom recording session with Jen from VFRS

endtime: 2124

nextdate: 2020-08-06
nexttime: "19:00"
nextplace: Zoom Teleconference System
---
