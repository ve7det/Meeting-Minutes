--- 
# ----- Beginning Details --------------------
team: VECTOR Board
date: 22 July 2020
day: Wednesday
chair: "Jessé Neri VE7DET"
time: "19:04"
location: on Zoom Teleconference System
purpose: To manage governance of the Society

# ----- Attendance --------------------
president: yes
secretary: yes
vicepresident: yes checking reminder about social distancing with fun
treasurer: yes	Anthony - signing authority at bank needs Jun meeting minutes
director1: no
director2: no
director3: yes  Prem - no vector
director4: yes  Darryl - away first 2 weeks of Aug
director5: yes	Linda - Vector Member letter approved by VEMA
members:
- None
external:
- None

# ----- Quorum --------------------
quorum:
- "7 board: Quorum at 19:10 (4 table officers and 3 directors at large)"
- "7 board members present from 19:04 to 20:05"

# ----- Confirming Agenda --------------------
agenda:
- first item
- second topic
- third subject
- fourth
- fifth
- sixth

# ----- Consent Agenda Notes --------------------
consentagendanotes:
- none
---

<!-- Remember that Agenda always starts with 5.1 = Approve Consent Agenda -->

### 5.2	First Subject

Advocate
: Anthony

Notes
: * line

<!---Line between sections-->

---

### 5.3 Second Subject
Advocate
: Tyson 

Initial
: I propose that... so that...

Notes
: * small reservation
* large reservation
* key idea

Revised
: I propose that... so that... and...

Moved
: Anthony

Seconded
: Tyson

Approved
: 4 of 5

<!---Line between sections-->

---

### 5.4	Third Subject

Advocate
: Jesse

Initial
: I propose 

Notes
: * Detailed 

DECISION
: Raise this to the members

ACTION: 
: Add the proposal statement to the agenda for the next member session
: Jesse
: 2020-07-29


---
# ----- CLOSING ITEMS --------------------
parkinglot:
- (all other items parked for next meeting)
- Directors' Liability Insurance
- Tester Insurance 
- Darryl indicates willing to take over managing website, Harondel requests that SOP be written for site management.
reflections:
- Jesse - concerns about lack of progress 
- Anthony - recommends discussing items over email between board meetings
- Linda - concerns about 
- Tyson - good that moved through weighty topics, good that Jesse wants to involve membership
- Harondel - happy medium between meetings and email in between
- Darryl - some amount of emails in between would be good to reduce discussion time during meeting
- Prem - glad got as far as we did, interviews with Fire and Police and how they surviced when commms went down, review manual for inter comms
- Linda - good meeting, lots to do and discussion, use email intermittently, need to come up with process to be more produtive at meetings
- Darryl requests Zoom recording session with Jen from VFRS

endtime: 2124

nextdate: 2020-08-06
nexttime: "19:00"
nextplace: Zoom Teleconference System
---
