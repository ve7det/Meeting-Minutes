FILE=202007xx-C3-2-V-BoardMeeting-Minutes-DRAFT
IN=content-freeform.md
PEOPLE=people
VER=A01
STYLE=style_meeting
STYLE_LTRHD=style_chmduquesne-vector-lhead
STYLES_DIR=styles
OUT_DIR=output
ID_DIR=identity


.PHONY: all pdf clean

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'


all: pdf letterhead markdown people ## Do all the steps


# Assemble markdown modules

markdown: letterhead.md $(FILE).md ## Assemble the markdown
	
letterhead.md: lAddress.md noFooter.md
	cat noFooter.md \
	lAddress.md > letterhead.md

$(FILE).md: $(IN) links.md
	cat \
	$(IN) links.md > $(FILE).md

# Join the People List to the Style-file

people: $(PEOPLE).tex $(STYLES_DIR)/$(STYLE)-complete.tex ## Assemble the core roster

$(STYLES_DIR)/$(STYLE)-complete.tex: 
	cat $(PEOPLE).tex \
	$(STYLES_DIR)/$(STYLE).tex > $(STYLES_DIR)/$(STYLE)-complete.tex
	
# Generate PDF letterhead

letterhead: $(ID_DIR)/letterhead.pdf letterhead.md ## Generate PDF letterhead

$(ID_DIR)/letterhead.pdf: $(STYLES_DIR)/$(STYLE_LTRHD).tex letterhead.md
	pandoc --standalone --template $(STYLES_DIR)/$(STYLE_LTRHD).tex \
	--from markdown --to context \
	-V papersize=letter \
	-o letterhead.tex letterhead.md; \
	context letterhead.tex; \
	cp letterhead.pdf $(ID_DIR)

# Generate PDF version of Document
# customize original template directly.  independent one needs serious work

pdf: $(FILE).pdf $(FILE).md ## Generate a PDF version of the document

$(FILE).pdf: $(STYLES_DIR)/$(STYLE)-complete.tex $(FILE).md $(ID_DIR)/letterhead.pdf
	pandoc --standalone --template $(STYLES_DIR)/$(STYLE)-complete.tex \
	--from markdown --to context \
	-V papersize=letter \
	-o $(FILE).tex $(FILE).md; \
	context $(FILE).tex; cp $(FILE).pdf $(OUT_DIR)/$(FILE)-$(VER).pdf



# Clean up the working and finished files

clean: tidy ## Clean up the working and finished files
	rm -f $(FILE).md
	rm -f $(FILE).pdf

tidy: ## Clean up the working files
	rm -f *.tuc
	rm -f *.log
	rm -f $(STYLES_DIR)/$(STYLE)-complete.tex
	rm -f letterhead.*
	rm -f $(ID_DIR)/letterhead.pdf
