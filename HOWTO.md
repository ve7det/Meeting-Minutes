# Record Meeting Summary

## Objective

Record essential information during a meeting so that our organization and teams can remember.  

## Goals

1. Record proposals and decisions
2. Record key points from the dialogue
3. Record actions and responsible actors


## Success Criteria for each Goal

### 1 - Record proposals and decisions
1. wording for the initiating proposal and final revised proposal recorded
2. decisions tagged "DECISION:" and recorded

### 2 - Record key points from the dialogue
1. points essential to understand the proposal, changes to the proposal, or the decision are captured
2. Only points where essential have tags marking which member spoke

### 3 - Record actions and responsible actors
1. actions tagged "ACTION:" and recorded with person performing it and due date

## Implementation Plan with Assumptions

### Step 1: Prepare

**Prepare the input files**

1. Recorder has downloaded the text file to place the notes into
2. Recorder has downloaded the working folder containing files needed as noted in the [README.md](README.md)_ file.  
2. Recorder has updated the [links.md](links.md) file to show links to the files for this meeting
3. Recorder has checked and updated the [people.tex](people.tex) file to correctly show the attendees
4. The [styles/style_meeting.tex](styles/style_meeting.tex) file has enough entries in the Roll Call section for the people in the people.tex list

**Step 2: Record**

**Record notes during the meeting**

1. Recorder will capture key information and ignore unnecessary parts
2. The only times important for the record are the start and finish

### Step 3: Proofread

**Proofread the input file**

1. Recorder captures typos and minor errors while meeting is fresh in their mind
2. Recorder will share the notes with another person to fill-in gaps and clarify items

### Step 4: Process

**Process the input file**

1. Recorder has the required software tools (noted in [README.md](README.md) present on their working computer
2. Recorder can run steps described in the README.md without errors

### Step 5: Review

**Review the output file**

1. Recorder has a PDF viewer
2. The [Makefile] successfully processed the input file  


### Step 6: Publish

**Publish the final output file**

1. All the links point to the correct files for the meeting
2. Step 5 worked

## Owner

Recorder or secretary for your team
