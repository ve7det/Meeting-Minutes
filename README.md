# Assembling a Polished Meeting Summary for VECTOR

We like to take notes using a rough-and-ready text editor.  Yet we need to show our meeting summaries in a "fully-dressed" format.  

Let's start using a Markdown-Pandoc method.  Edit the raw notes into context.md without concern for formatting and we can use Make to call Pandoc to create a PDF file using ConTeXt.  

## Table of Contents

* [Usage](#usage)
* [Installation](#installation)
* [Uses](#uses)
* [Needs](#needs)
* [Contributing](#contributing)
* [Credits](#credits)
* [License](#license)
* [Donations](#donations)

## Usage

This project follows the model set by Pandoc-Resume and Pandoc-Letter. 

1. Enter your meeting information in the **content.md** file.  The entries follow YAML syntax and the ConText engine uses these with the style-file to create the PDF.  

2. Review and revise the options in the Makefile variables to suit your file and formatting.  The file **style_meeting.tex** is set up for 2020 VECTOR.  
    - output name
    - style-file name

3. Run make in the working folder to format the information and assemble the final file.  (will work on binding in the consent agenda through **make**.)

```shell
$ make
```


## Installation

1. Download the repository
2. Extract the files into a working folder

## Uses

* Context
* Pandoc
* PDFTK
* Ghostscript
* Automake

## Needs

Needs TEX templates in the styles folder and your meeting notes in **content.md** using the YAML system shown in the file.  

## Contributing

Welcome to, pretty informal.  

## Credits

* Jesse VE7DET

## License

Looking at Creative Commons Share and Share Alike.  

## Donations

We accept donations at the [VECTOR website](https://vectorradio.ca/donate/)